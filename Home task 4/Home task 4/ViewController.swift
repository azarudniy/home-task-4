//
//  ViewController.swift
//  Home task 4
//
//  Created by Александр Зарудний on 9/29/21.
//

import UIKit
import SnapKit
import MapKit
import CoreLocation

class ViewController: UIViewController {
    
    private var isFirstTime = true
    
    lazy var mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        mapView.delegate = self
        return mapView
    }()

    let locationManager = CLLocationManager()
    var userLocation = CLLocation()
    var arrayBankEntity = [BankEntity]()
    
    private let urlStringATM = "https://belarusbank.by/api/atm?city=Гомель"
    private let urlStringTerminals = "https://belarusbank.by/api/infobox?city=Гомель"
    private let urlStringCashCenters = "https://belarusbank.by/api/filials_info?city=Гомель"
    
    
    //MARK: ~viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(mapView)
        mapView.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.width.height.equalToSuperview()
        }
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        checkLocationAuthorization()
        
        if let urlStringATMEncoded = urlStringATM.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
           let urlStringTerminalsEncoded = urlStringTerminals.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
           let urlStringCashCentersEncoded = urlStringCashCenters.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
           let urlATM = URL(string: urlStringATMEncoded),
           let urlTerminals = URL(string: urlStringTerminalsEncoded),
           let urlCashCenters = URL(string: urlStringCashCentersEncoded) {
            let arrayURL: [URL] = [urlATM, urlTerminals, urlCashCenters]
            self.asyncParsingData(arrayURL: arrayURL)
        }
    }

    //MARK: ~set map center
    func checkLocationAuthorization() {
        switch locationManager.authorizationStatus {
        case .authorizedWhenInUse, .authorizedAlways :
            mapView.showsUserLocation = true
            followUserLocation()
            break
        case .denied, .notDetermined, .restricted:
            let location = CLLocationCoordinate2D(latitude: 52.431233, longitude: 30.992697)
            let span = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
            let region = MKCoordinateRegion(center: location, span: span)
            mapView.setRegion(region, animated: true)
            break
        default:
            break
        }
        locationManager.startUpdatingLocation()
    }

    //MARK: ~set map on user location
    func followUserLocation() {
        if let location = locationManager.location?.coordinate {
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: 4000, longitudinalMeters: 4000)
            self.setPins(arrayBankEntity: self.arrayBankEntity)
            mapView.setRegion(region, animated: true)
        }
    }
    
    //MARK: ~parsing URL data
    func parsingATMData(arrayURL: [URL]) {
        var arrayBankEntity = [BankEntity]()
        for url in arrayURL {
            DispatchQueue.global().async {
                URLSession.shared.dataTask(with: url) { (data, response, error) in
                    if error != nil {
                        return
                    }
                    
                    do {
                        if let data = data {
                            try arrayBankEntity.append(contentsOf: JSONDecoder().decode([BankEntity].self, from: data))
                            print(arrayBankEntity.count)
                        }
                    } catch {
                        return
                    }
                    DispatchQueue.main.async {
                        self.setPins(arrayBankEntity: arrayBankEntity)
                        
                    }
                }.resume()
            }
        }
    }
    
    //MARK: ~sync parsing URL data
    func syncParsingData(arrayURL: [URL]) {
        let queue = DispatchQueue(label: "serial queue")
        for url in arrayURL {
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                Thread.sleep(until: Date() + 10)
                if error != nil {
                    return
                }
                queue.async {
                    do {
                        if let data = data {
                            try self.arrayBankEntity.append(contentsOf: JSONDecoder().decode([BankEntity].self, from: data))
                            DispatchQueue.main.async {
                                self.setPins(arrayBankEntity: self.arrayBankEntity)
                            }
                        }
                    } catch {
                        return
                    }
                }
            }.resume()
        }
    }
    
    //MARK: ~async parsing URL data
    func asyncParsingData(arrayURL: [URL]) {
        let group = DispatchGroup()
        for url in arrayURL {
            group.enter()
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    group.leave()
                    return
                }
                do {
                    if let data = data {
                        try self.arrayBankEntity.append(contentsOf: JSONDecoder().decode([BankEntity].self, from: data))
                    }
                } catch {
                    group.leave()
                    return
                }
                group.leave()
            }.resume()
        }
        group.notify(queue: .main) {
            print(self.arrayBankEntity.count)
            self.setPins(arrayBankEntity: self.arrayBankEntity)
        }
    }
    
    //MARK: ~set pins
    func setPins(arrayBankEntity: [BankEntity]) {
        self.mapView.removeAnnotations(mapView.annotations)
        var annotations = [MKPointAnnotation]()
        for bankEntity in self.sortLocationsBankEntities(arrayBankEntity: arrayBankEntity) {
            let annotation = MKPointAnnotation()
            if let latitude = Double(bankEntity.gps_x), let longitude = Double(bankEntity.gps_y) {
                annotation.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            } else {
            }
            annotations.append(annotation)
        }
        mapView.addAnnotations(annotations)
    }
    
    //MARK: ~sort arrayBankEntity
    func sortLocationsBankEntities(arrayBankEntity: [BankEntity]) -> [BankEntity] {
        if arrayBankEntity.count < 10 {
            return arrayBankEntity
        }
        return Array(arrayBankEntity.sorted {
            self.getDistance(bankEntity: $0) < self.getDistance(bankEntity: $1)
        }[0...10])
    }
    
    func getDistance(bankEntity: BankEntity) -> Double {
        var location = CLLocation()
        if let latitude = Double(bankEntity.gps_x), let longitude = Double(bankEntity.gps_y) {
             location = CLLocation(latitude: latitude, longitude: longitude)
        }
        
        if CLLocationManager.locationServicesEnabled() {
            switch locationManager.authorizationStatus {
            case .authorizedAlways, .authorizedWhenInUse:
                guard let userLocation = locationManager.location else {
                    print("Wrong user location")
                    return 0
                }
                return location.distance(from: userLocation)
            case .denied, .notDetermined, .restricted:
                return location.distance(from: CLLocation(latitude: 52.431233, longitude: 30.992697))
            default:
                print("Error in Geolocation status")
                return 0
            }
        } else {
            return location.distance(from: CLLocation(latitude: 52.425163, longitude: 31.015039))
        }
    }
}

//MARK: ~locationManager delegate
extension ViewController: CLLocationManagerDelegate, MKMapViewDelegate {
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        checkLocationAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if isFirstTime {
            isFirstTime = false
            guard let location = locations.first else { return }
            let region = MKCoordinateRegion.init(center: location.coordinate, latitudinalMeters: 4000, longitudinalMeters: 4000)
            mapView.setRegion(region, animated: true)
        }
        if let location = manager.location {
            self.userLocation = location
        }
    }
    //MARK: ~mapView delegate
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
    }
}

struct BankEntity: Codable {
    var gps_x: String
    var gps_y: String
    
    enum CodingKeys1: String, CodingKey {
        case gps_x
        case gps_y
    }
    
    enum CodingKeys2: String, CodingKey {
        case gps_x = "GPS_X"
        case gps_y = "GPS_Y"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: CodingKeys1.self)
            try self.init(container)
        } catch {
            let container = try decoder.container(keyedBy: CodingKeys2.self)
            try self.init(container)
        }
    }
    
    private init(_ container: KeyedDecodingContainer<CodingKeys1>) throws {
        gps_x = try container.decode(String.self, forKey: .gps_x)
        gps_y = try container.decode(String.self, forKey: .gps_y)
    }
    
    private init(_ container: KeyedDecodingContainer<CodingKeys2>) throws {
        gps_x = try container.decode(String.self, forKey: .gps_x)
        gps_y = try container.decode(String.self, forKey: .gps_y)
    }
}
